<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>登录</title>
    <link rel="stylesheet" href="../../static/frame/layui/css/layui.css">
    <link rel="stylesheet" href="../../static/css/style.css">
    <link rel="icon" href="../image/code.png">
</head>
<body class="login-body body">

<div class="login-box">
    <form class="layui-form layui-form-pane" method="post" action="admin/login.html" id="login_form">
        <div class="layui-form-item">
            <h3>家乐童-后台登录系统</h3>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">账号：</label>

            <div class="layui-input-inline">
                <input type="text" name="account" id="login-account" class="layui-input" lay-verify="account" placeholder="账号" autocomplete="on" maxlength="20"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码：</label>

            <div class="layui-input-inline">
                <input type="password" name="password" id="login-password" class="layui-input" lay-verify="password" placeholder="密码" maxlength="20"/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">验证码：</label>

            <div class="layui-input-inline">
                <input type="number" name="code" id="login-code" class="layui-input" lay-verify="code" placeholder="验证码" maxlength="4"/><img src="authCode.html" alt="">
            </div>
        </div>
        <div class="layui-form-item">
            <button type="reset" class="layui-btn layui-btn-danger btn-reset">重置</button>
            <button type="button" id="submit-form-login" class="layui-btn btn-submit" lay-submit="" lay-filter="sub">立即登录</button>
        </div>
    </form>
</div>
<script type="text/javascript" src="../../static/js/jquery-3.2.1.min.js" ></script>

<script type="text/javascript" src="../../static/frame/layui/layui.js"></script>
<!--<script type="text/javascript" src="../js/common.js"></script>-->
<script type="text/javascript">
    
     layui.use(['form', 'layer'], function () {
        var $ = layui.jquery,form = layui.form(),layer = layui.layer;
        // 验证
        form.verify({
            account: function (value) {
                if (value == "") {
                    return "请输入用户名";
                }
            },
            password: function (value) {
                if (value == "") {
                    return "请输入密码";
                }
            },
            code: function (value) {
                if (value == "") {
                    return "请输入验证码";
                }
            }
        });
        // 提交监听
        form.on('submit(sub)', function (data) {
            load = layer.load();
//            layer.alert(JSON.stringify(data.field), {
//                title: '最终的提交信息'
//            });
             console.log(data);
            $.ajax({
                url:"../admin/login.html",
                type:"POST",
                data:data.field,
                success:function(data){
                     alert(data);
                    layer.close(load);
                },
                error:function(){
                    layer.close(load);
                }
            });
            return false;
        })
    })
    
    
    
    
// $(document).ready(function(){
//     var layer = 
//     $("#submit-form-login").click(function(){
//         load = layer.load();
//         var code= $("#login-code").val();
//         var password = $("#login-password").val();
//         var account = $("#login-account").val();
//         if(account == ""){
//             layer.msg('登陆帐号不能为空', {icon: 2});
//             return false;
//         }
//         if(password == ""){
//             layer.msg('登陆密码不能为空', {icon: 2});
//             return false;
//         }
//         if(code == ""){
//             layer.msg('登陆验证码不能为空', {icon: 2});
//             return false;
//         }
//         var params={cdoe:code,account:account,password:password};
//        $.ajax({
//            url:"admin/login.html",
//            type:"POST",
//            data:params,
//            success:function(data){
//                 alert(data);
//                layer.close(load);
//            },
//            error:function(){
//                layer.close(load);
//            }
//         });
//     });
//});

</script>
</body>
</html>
