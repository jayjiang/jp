/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : baijiacms

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-04-15 17:04:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- --------------------------------
-- 管理员表
-- --------------------------------
-- DROP TABLE IF EXISTS `admin`;
-- CREATE TABLE IF EXISTS `admin`(
--     `id` int(10) unsigned NOT NULL COMMENT '管理员ID',
--     `role_id` int(10) unsigned not null COMMENT '角色ID',
--     `group_id` int(10) unsigned not null COMMENT '用户组ID',
--     ``
-- )
-- 
-- DROP TABLE IF EXISTS `user_role`;  
-- CREATE TABLE `user_role` (  
--   `user_id` bigint(20) NOT NULL,  
--   `role_id` bigint(20) NOT NULL,  
--   PRIMARY KEY (`user_id`,`role_id`),  
--   KEY `FK3kaylpusfxvtsulifg7u1o0cj` (`role_id`),  
--   CONSTRAINT `FK3kaylpusfxvtsulifg7u1o0cj` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),  
--   CONSTRAINT `FK6520i3b07huaey0kr9fw49ln` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)  
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
-- 
-- DROP TABLE IF EXISTS `permission_menu`;  
-- CREATE TABLE `permission_menu` (  
--   `permission_id` bigint(20) NOT NULL,  
--   `menu_id` bigint(20) NOT NULL,  
--   PRIMARY KEY (`permission_id`,`menu_id`),  
--   KEY `FKrck475a4xibvhvbipdsbml4jo` (`menu_id`),  
--   CONSTRAINT `FK892mp1voq26r0krjfnoqqfy8e` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`),  
--   CONSTRAINT `FKrck475a4xibvhvbipdsbml4jo` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`)  
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
-- 
-- DROP TABLE IF EXISTS `role_permission`;  
-- CREATE TABLE `role_permission` (  
--   `role_id` bigint(20) NOT NULL,  
--   `permission_id` bigint(20) NOT NULL,  
--   PRIMARY KEY (`role_id`,`permission_id`),  
--   KEY `FKjsv718s6pysaxl3hwdbh32v16` (`permission_id`),  
--   CONSTRAINT `FK71nkax6g3ndcd1q4iue3xfksb` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`),  
--   CONSTRAINT `FKjsv718s6pysaxl3hwdbh32v16` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`id`)  
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;  
-- 
-- DROP TABLE IF EXISTS `usergroup_role`;  
-- CREATE TABLE `usergroup_role` (  
--   `usergroup_id` bigint(20) NOT NULL,  
--   `role_id` bigint(20) NOT NULL,  
--   PRIMARY KEY (`usergroup_id`,`role_id`),  
--   KEY `FKecy0fvxdsy58ku64uhsd1y1qd` (`role_id`),  
--   CONSTRAINT `FKecy0fvxdsy58ku64uhsd1y1qd` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),  
--   CONSTRAINT `FKksc98i8kfc50g8bo4imvep8uk` FOREIGN KEY (`usergroup_id`) REFERENCES `usergroup` (`id`)  
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 



CREATE TABLE `employee` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`role_id` int(10) not null default 0 comment '角色ID',
`group_id` int(10) not null default 0 comment '用户组ID',
`usernname`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ' ' COMMENT '工作人员登陆帐号' ,
`password`  char(100) NOT NULL DEFAULT ' ' COMMENT '员工登陆密码' ,
`mobile`  char(20) NOT NULL DEFAULT ' ' COMMENT '电话号码' ,
`status`  int(1) NOT NULL DEFAULT 0 COMMENT '状态' ,
`nicname`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ' ' COMMENT '名称' ,
`email`  char(200) NULL ,
`secret`  char(100) NULL ,
`updated_time`  int(10)  not NULL default 0 ,
`created_time`  int(10) not NULL default 0,
PRIMARY KEY (`id`)
);


CREATE TABLE `group` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`role_id`  int(10) NULL ,
`name`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ' ' COMMENT '工作人员登陆帐号' ,
`displayName `  char(100) NOT NULL DEFAULT ' ' COMMENT '员工登陆密码' ,
`description `  char(200) NOT NULL DEFAULT ' ' COMMENT '电话号码' ,
`updated_time`  int(10)  not NULL default 0 ,
`created_time`  int(10) not NULL default 0,
PRIMARY KEY (`id`)
);

CREATE TABLE `role`  
(
    `id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `name` character varying(255) UNIQUE NOT NULL,
    `displayName` character varying(255),
    `description` character varying(255),
    `updated_time`  int(10)  not NULL default 0 ,
    `created_time`  int(10) not NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `permission` 
(
    `id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
    `name` character varying(255) UNIQUE NOT NULL,
    `description` character varying(255),
    `updated_time`  int(10)  not NULL default 0 ,
    `created_time`  int(10) not NULL default 0,
    PRIMARY KEY (`id`)
);





-- ----------------------------
-- Table structure for `addon7_award`
-- ----------------------------
DROP TABLE IF EXISTS `addon7_award`;
CREATE TABLE `addon7_award` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `endtime` int(10) NOT NULL,
  `awardtype` int(1) NOT NULL DEFAULT '0',
  `gold` decimal(10,2) NOT NULL DEFAULT '0.00',
  `credit_cost` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '100',
  `content` text NOT NULL,
  `createtime` int(10) NOT NULL,
  `deleted` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon7_award
-- ----------------------------

-- ----------------------------
-- Table structure for `addon7_config`
-- ----------------------------
DROP TABLE IF EXISTS `addon7_config`;
CREATE TABLE `addon7_config` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon7_config
-- ----------------------------

-- ----------------------------
-- Table structure for `addon7_request`
-- ----------------------------
DROP TABLE IF EXISTS `addon7_request`;
CREATE TABLE `addon7_request` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL,
  `realname` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `award_id` int(10) unsigned NOT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addon7_request
-- ----------------------------

-- ----------------------------
-- Table structure for `alipay_alifans`
-- ----------------------------
DROP TABLE IF EXISTS `alipay_alifans`;
CREATE TABLE `alipay_alifans` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL DEFAULT '0',
  `openid` varchar(50) DEFAULT NULL,
  `alipay_openid` varchar(50) NOT NULL,
  `follow` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否订阅',
  `nickname` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(200) NOT NULL DEFAULT '',
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别(0:保密 1:男 2:女)',
  PRIMARY KEY (`beid`,`alipay_openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alipay_alifans
-- ----------------------------

-- ----------------------------
-- Table structure for `alipay_rule`
-- ----------------------------
DROP TABLE IF EXISTS `alipay_rule`;
CREATE TABLE `alipay_rule` (
  `beid` int(10) NOT NULL,
  `url` varchar(500) NOT NULL,
  `thumb` varchar(60) NOT NULL,
  `keywords` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `ruletype` int(11) NOT NULL COMMENT '1文本回复 2图文回复',
  `content` text,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alipay_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `attachment`
-- ----------------------------
DROP TABLE IF EXISTS `attachment`;
CREATE TABLE `attachment` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL COMMENT '1为图片',
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `bj_tbk_diyshopindex`
-- ----------------------------
DROP TABLE IF EXISTS `bj_tbk_diyshopindex`;
CREATE TABLE `bj_tbk_diyshopindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) NOT NULL,
  `beid` int(10) NOT NULL,
  `pagename` varchar(255) NOT NULL DEFAULT '' COMMENT '页面名称',
  `pagetype` tinyint(3) NOT NULL DEFAULT '0' COMMENT '页面类型 1首页，0其他',
  `pageinfo` text NOT NULL,
  `createtime` varchar(255) NOT NULL DEFAULT '' COMMENT '页面创建时间',
  `updatetime` varchar(255) NOT NULL DEFAULT '' COMMENT '页面最后保存时间',
  `showtype` tinyint(2) NOT NULL DEFAULT '0' COMMENT '页面类型0DIY页面1html代码页面',
  `datas` text NOT NULL COMMENT '数据',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bj_tbk_diyshopindex
-- ----------------------------

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(100) NOT NULL COMMENT '配置名称',
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', '1', 'shop_openreg', '1');
INSERT INTO `config` VALUES ('1', '2', 'system_config_cache', 'a:1:{s:12:\"shop_openreg\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for `dispatch`
-- ----------------------------
DROP TABLE IF EXISTS `dispatch`;
CREATE TABLE `dispatch` (
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '0店铺1总部',
  `beid` int(10) NOT NULL,
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(120) NOT NULL DEFAULT '',
  `sendtype` int(5) NOT NULL DEFAULT '1' COMMENT '0为快递，1为自提',
  `desc` text NOT NULL,
  `configs` text NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dispatch
-- ----------------------------

-- ----------------------------
-- Table structure for `gold_order`
-- ----------------------------
DROP TABLE IF EXISTS `gold_order`;
CREATE TABLE `gold_order` (
  `weixin_transaction_openid` varchar(50) DEFAULT '',
  `weixin_transaction_id` varchar(100) DEFAULT '',
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `paytime` int(10) DEFAULT '0' COMMENT '支付时间',
  `price` decimal(10,2) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `ordersn` varchar(20) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gold_order
-- ----------------------------

-- ----------------------------
-- Table structure for `gold_teller`
-- ----------------------------
DROP TABLE IF EXISTS `gold_teller`;
CREATE TABLE `gold_teller` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '0未审核-1拒绝1审核功成',
  `fee` decimal(10,2) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `ordersn` varchar(20) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gold_teller
-- ----------------------------

-- ----------------------------
-- Table structure for `member`
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `beid` int(10) NOT NULL,
  `weixinhao` varchar(50) DEFAULT '',
  `email` varchar(20) NOT NULL,
  `credit` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `gold` double NOT NULL DEFAULT '0' COMMENT '余额',
  `openid` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `nickname` varchar(30) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `createtime` int(10) NOT NULL,
  `istemplate` tinyint(1) DEFAULT '0' COMMENT '是否为临时账户 1是，0为否',
  `status` tinyint(1) DEFAULT '1' COMMENT '0为禁用，1为可用',
  `experience` int(11) DEFAULT '0' COMMENT '账户经验值',
  `avatar` varchar(200) DEFAULT '' COMMENT '用户头像',
  `outgold` double NOT NULL DEFAULT '0' COMMENT '已提取余额',
  `outgoldinfo` varchar(1000) DEFAULT '0' COMMENT '提款信息 序列化',
  PRIMARY KEY (`openid`),
  KEY `idx_member_from_user` (`openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', '', '', '0', '0', '20170404164795708682', '', '', '18025363002', '3f4c39629418596f129fcaa5c8142a33', '1491293318', '0', '1', '0', '', '0', '0');

-- ----------------------------
-- Table structure for `member_paylog`
-- ----------------------------
DROP TABLE IF EXISTS `member_paylog`;
CREATE TABLE `member_paylog` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `remark` varchar(100) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `type` varchar(30) NOT NULL COMMENT 'usegold使用金额 addgold充值金额 usecredit使用积分 addcredit充值积分',
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_fee` decimal(10,2) NOT NULL COMMENT '账户剩余积分或余额',
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of member_paylog
-- ----------------------------

-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `displayorder` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(30) NOT NULL,
  `group` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `version` decimal(5,2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `isdisable` int(1) DEFAULT '0' COMMENT '模块是否禁用',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('0', 'icon-money', 'addons', '积分兑换', '1.00', 'addon7', '0');

-- ----------------------------
-- Table structure for `modules_menu`
-- ----------------------------
DROP TABLE IF EXISTS `modules_menu`;
CREATE TABLE `modules_menu` (
  `href` varchar(200) NOT NULL,
  `title` varchar(50) NOT NULL,
  `module` varchar(30) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of modules_menu
-- ----------------------------
INSERT INTO `modules_menu` VALUES ('index.php?mod=site&name=addon7&do=setting', '参数设置', 'addon7', '1');
INSERT INTO `modules_menu` VALUES ('index.php?mod=site&name=addon7&do=addaward', '添加积分商品', 'addon7', '2');
INSERT INTO `modules_menu` VALUES ('index.php?mod=site&name=addon7&do=awardlist', '积分商品列表', 'addon7', '3');
INSERT INTO `modules_menu` VALUES ('index.php?mod=site&name=addon7&do=applyed', '兑换申请列表', 'addon7', '4');

-- ----------------------------
-- Table structure for `paylog`
-- ----------------------------
DROP TABLE IF EXISTS `paylog`;
CREATE TABLE `paylog` (
  `beid` int(10) NOT NULL,
  `paytype` varchar(30) NOT NULL,
  `pdate` text NOT NULL,
  `ptype` varchar(10) NOT NULL,
  `typename` varchar(30) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paylog
-- ----------------------------

-- ----------------------------
-- Table structure for `paylog_alipay`
-- ----------------------------
DROP TABLE IF EXISTS `paylog_alipay`;
CREATE TABLE `paylog_alipay` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `alipay_safepid` varchar(50) DEFAULT NULL,
  `buyer_email` varchar(50) DEFAULT NULL,
  `buyer_id` varchar(50) DEFAULT NULL,
  `out_trade_no` varchar(50) DEFAULT NULL,
  `seller_email` varchar(50) DEFAULT NULL,
  `seller_id` varchar(50) DEFAULT NULL,
  `total_fee` decimal(10,2) DEFAULT NULL COMMENT '交易金额',
  `trade_no` varchar(50) DEFAULT NULL,
  `body` varchar(200) DEFAULT NULL,
  `orderid` int(10) DEFAULT NULL,
  `ordersn` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `presult` varchar(50) DEFAULT NULL COMMENT 'success 或error',
  `order_table` varchar(50) DEFAULT NULL COMMENT '订单类型 shop_order gold_order',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paylog_alipay
-- ----------------------------

-- ----------------------------
-- Table structure for `paylog_unionpay`
-- ----------------------------
DROP TABLE IF EXISTS `paylog_unionpay`;
CREATE TABLE `paylog_unionpay` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `txnTime` int(10) DEFAULT NULL,
  `txnAmt` decimal(10,2) DEFAULT NULL COMMENT '交易金额',
  `queryid` varchar(50) DEFAULT NULL COMMENT '交易查询流水号',
  `currencyCode` varchar(10) DEFAULT NULL COMMENT '交易币种',
  `reqReserved` varchar(100) DEFAULT NULL COMMENT '请求保留域',
  `settleAmt` decimal(10,2) DEFAULT NULL COMMENT '清算金额',
  `settleCurrencyCode` varchar(10) DEFAULT NULL COMMENT '清算币种',
  `traceTime` int(10) DEFAULT NULL COMMENT '交易传输时间',
  `traceNo` varchar(50) DEFAULT NULL COMMENT '系统跟踪号',
  `merId` varchar(50) DEFAULT NULL COMMENT '商户代码',
  `orderid` int(10) DEFAULT NULL,
  `ordersn` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `presult` varchar(50) DEFAULT NULL COMMENT 'success 或error',
  `order_table` varchar(50) DEFAULT NULL COMMENT '订单类型 shop_order gold_order',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paylog_unionpay
-- ----------------------------

-- ----------------------------
-- Table structure for `paylog_weixin`
-- ----------------------------
DROP TABLE IF EXISTS `paylog_weixin`;
CREATE TABLE `paylog_weixin` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `timeend` int(10) DEFAULT NULL,
  `total_fee` decimal(10,2) DEFAULT NULL COMMENT '交易金额',
  `mchId` varchar(50) DEFAULT NULL COMMENT '商户id',
  `openid` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `out_trade_no` varchar(50) DEFAULT NULL,
  `orderid` int(10) DEFAULT NULL,
  `ordersn` varchar(50) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `presult` varchar(50) DEFAULT NULL COMMENT 'success 或error',
  `order_table` varchar(50) DEFAULT NULL COMMENT '订单类型 shop_order gold_order',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paylog_weixin
-- ----------------------------

-- ----------------------------
-- Table structure for `payment`
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(120) NOT NULL DEFAULT '',
  `desc` text NOT NULL,
  `order` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `configs` text NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `iscod` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `beid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment
-- ----------------------------

-- ----------------------------
-- Table structure for `qq_qqfans`
-- ----------------------------
DROP TABLE IF EXISTS `qq_qqfans`;
CREATE TABLE `qq_qqfans` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL DEFAULT '0',
  `openid` varchar(50) DEFAULT NULL,
  `qq_openid` varchar(50) NOT NULL,
  `nickname` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(200) NOT NULL DEFAULT '',
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别(0:保密 1:男 2:女)',
  PRIMARY KEY (`beid`,`qq_openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qq_qqfans
-- ----------------------------

-- ----------------------------
-- Table structure for `rank_model`
-- ----------------------------
DROP TABLE IF EXISTS `rank_model`;
CREATE TABLE `rank_model` (
  `beid` int(10) NOT NULL,
  `experience` int(11) DEFAULT '0',
  `rank_level` int(3) NOT NULL DEFAULT '0' COMMENT '等级',
  `rank_name` varchar(50) DEFAULT NULL COMMENT '等级名称',
  PRIMARY KEY (`rank_level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rank_model
-- ----------------------------

-- ----------------------------
-- Table structure for `rank_phb`
-- ----------------------------
DROP TABLE IF EXISTS `rank_phb`;
CREATE TABLE `rank_phb` (
  `beid` int(10) NOT NULL,
  `rank_level` int(11) DEFAULT '0',
  `rank_name` varchar(50) DEFAULT '',
  `realname` varchar(50) NOT NULL DEFAULT '',
  `openid` varchar(50) NOT NULL DEFAULT '',
  `rank_top` int(2) NOT NULL DEFAULT '0' COMMENT '名次',
  PRIMARY KEY (`rank_top`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rank_phb
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_address`
-- ----------------------------
DROP TABLE IF EXISTS `shop_address`;
CREATE TABLE `shop_address` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `area` varchar(30) NOT NULL,
  `address` varchar(300) NOT NULL,
  `isdefault` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_address
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_adv`
-- ----------------------------
DROP TABLE IF EXISTS `shop_adv`;
CREATE TABLE `shop_adv` (
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_enabled` (`enabled`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_adv
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_cart`
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `goodstype` tinyint(1) NOT NULL DEFAULT '1',
  `session_id` varchar(50) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `optionid` int(10) DEFAULT '0',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `idx_openid` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_cart
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_category`
-- ----------------------------
DROP TABLE IF EXISTS `shop_category`;
CREATE TABLE `shop_category` (
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '0为普通商品，1为总部商品',
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `commission` int(10) unsigned DEFAULT '0' COMMENT '推荐该类商品所能获得的佣金',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `thumb` varchar(255) NOT NULL COMMENT '分类图片',
  `thumbadv` varchar(255) NOT NULL COMMENT '分类广告图片',
  `thumbadvurl` varchar(255) NOT NULL COMMENT '分类广告url',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID,0为第一级',
  `isrecommand` int(10) DEFAULT '0',
  `description` varchar(500) NOT NULL COMMENT '分类介绍',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_category
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_dispatch`
-- ----------------------------
DROP TABLE IF EXISTS `shop_dispatch`;
CREATE TABLE `shop_dispatch` (
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '0店铺1总部',
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispatchname` varchar(50) NOT NULL,
  `sendtype` int(5) NOT NULL DEFAULT '1' COMMENT '0为快递，1为自提',
  `firstprice` decimal(10,2) NOT NULL,
  `secondprice` decimal(10,2) NOT NULL,
  `provance` varchar(30) DEFAULT '',
  `city` varchar(30) DEFAULT '',
  `area` varchar(30) DEFAULT '',
  `firstweight` int(10) NOT NULL,
  `secondweight` int(10) NOT NULL,
  `express` varchar(50) NOT NULL,
  `deleted` int(10) NOT NULL DEFAULT '0',
  `displayorder` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_dispatch
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_dispatch_area`
-- ----------------------------
DROP TABLE IF EXISTS `shop_dispatch_area`;
CREATE TABLE `shop_dispatch_area` (
  `is_system` int(1) NOT NULL DEFAULT '0',
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dispatchid` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `provance` varchar(30) DEFAULT '',
  `city` varchar(30) DEFAULT '',
  `area` varchar(30) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_dispatch_area
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_diymenu`
-- ----------------------------
DROP TABLE IF EXISTS `shop_diymenu`;
CREATE TABLE `shop_diymenu` (
  `beid` int(10) NOT NULL,
  `menu_type` varchar(10) NOT NULL,
  `torder` int(2) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `url` varchar(350) NOT NULL,
  `tname` varchar(100) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_diymenu
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE `shop_goods` (
  `goodsFHprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `goodsFHtype` int(1) NOT NULL DEFAULT '0',
  `beid` int(10) NOT NULL,
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '0为普通商品，1为总部商品',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pcate` int(10) unsigned NOT NULL DEFAULT '0',
  `ccate` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0为实体，1为虚拟',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `displayorder` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `goodssn` varchar(50) NOT NULL DEFAULT '',
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `productsn` varchar(50) NOT NULL DEFAULT '',
  `marketprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `productprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` int(10) NOT NULL DEFAULT '0',
  `totalcnf` int(11) DEFAULT '0' COMMENT '0 拍下减库存 1 付款减库存 2 永久不减',
  `sales` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `credit` int(11) DEFAULT '0',
  `hasoption` int(11) DEFAULT '0',
  `isnew` int(11) DEFAULT '0',
  `issendfree` int(11) DEFAULT NULL,
  `ishot` int(11) DEFAULT '0',
  `isdiscount` int(11) DEFAULT '0',
  `isrecommand` int(11) DEFAULT '0',
  `istime` int(11) DEFAULT '0',
  `timestart` int(11) DEFAULT '0',
  `timeend` int(11) DEFAULT '0',
  `viewcount` int(11) DEFAULT '0',
  `remark` text,
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `isfirst` int(1) DEFAULT '0' COMMENT '首发',
  `isjingping` int(1) DEFAULT '0' COMMENT '精品',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_comment`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_comment`;
CREATE TABLE `shop_goods_comment` (
  `beid` int(10) NOT NULL,
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '0为普通商品，1为总部商品',
  `createtime` int(10) NOT NULL,
  `optionname` varchar(100) DEFAULT NULL,
  `orderid` int(10) DEFAULT NULL,
  `ordersn` varchar(20) DEFAULT NULL,
  `comment_nickname` varchar(100) DEFAULT NULL,
  `isenable` int(1) DEFAULT '0' COMMENT '0未审核,1审核通过',
  `openid` varchar(50) DEFAULT NULL,
  `comment` text,
  `rate` int(1) DEFAULT '0' COMMENT '0差评 1中评 2好评',
  `goodsid` int(10) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_goodsstore`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_goodsstore`;
CREATE TABLE `shop_goods_goodsstore` (
  `ccate` int(10) NOT NULL,
  `pcate` int(10) NOT NULL,
  `good_id` int(10) NOT NULL,
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_goodsstore
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_option`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_option`;
CREATE TABLE `shop_goods_option` (
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(10) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `thumb` varchar(60) DEFAULT '',
  `productprice` decimal(10,2) DEFAULT '0.00',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `costprice` decimal(10,2) DEFAULT '0.00',
  `stock` int(11) DEFAULT '0',
  `weight` decimal(10,2) DEFAULT '0.00',
  `displayorder` int(11) DEFAULT '0',
  `specs` text,
  PRIMARY KEY (`id`),
  KEY `indx_goodsid` (`goodsid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_option
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_piclist`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_piclist`;
CREATE TABLE `shop_goods_piclist` (
  `beid` int(10) NOT NULL,
  `picurl` varchar(255) NOT NULL,
  `goodid` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_piclist
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_spec`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_spec`;
CREATE TABLE `shop_goods_spec` (
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `displaytype` tinyint(3) unsigned NOT NULL,
  `content` text NOT NULL,
  `goodsid` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_spec
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_goods_spec_item`
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods_spec_item`;
CREATE TABLE `shop_goods_spec_item` (
  `beid` int(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `specid` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `show` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_specid` (`specid`),
  KEY `indx_show` (`show`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_goods_spec_item
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_order`
-- ----------------------------
DROP TABLE IF EXISTS `shop_order`;
CREATE TABLE `shop_order` (
  `weixin_transaction_openid` varchar(50) DEFAULT '',
  `weixin_transaction_id` varchar(100) DEFAULT '',
  `zong_hasrest` int(1) NOT NULL COMMENT '0正常  1部分商品退换货',
  `be_hasrest` int(1) NOT NULL COMMENT '0正常 1部分商品退换货',
  `zong_updatetime` int(10) DEFAULT '0' COMMENT '订单更新时间',
  `be_updatetime` int(10) DEFAULT '0' COMMENT '订单更新时间',
  `zong_ordersn` varchar(20) DEFAULT '',
  `be_ordersn` varchar(20) DEFAULT '',
  `be_returnmoney` decimal(10,2) NOT NULL DEFAULT '0.00',
  `be_returnmoneytype` int(1) NOT NULL,
  `zong_returnmoney` decimal(10,2) NOT NULL DEFAULT '0.00',
  `zong_returnmoneytype` int(1) NOT NULL,
  `is_system_fh` int(1) NOT NULL,
  `compid` int(10) NOT NULL,
  `saleid` int(10) NOT NULL,
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL,
  `ordersn` varchar(20) NOT NULL,
  `oldordersn` varchar(20) NOT NULL,
  `credit` int(10) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-6已退款 -5已退货 -4退货中， -3换货中， -2退款中，-1取消状态，0普通状态，1为已付款，2为已发货，3为成功',
  `be_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '分部发货状态 -6已退款 -5已退货 -4退货中， -3换货中， -2退款中，-1取消状态，0普通状态，1为已付款，2为已发货，3为成功',
  `zong_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '总部发货状态 -6已退款 -5已退货 -4退货中， -3换货中， -2退款中，-1取消状态，0普通状态，1为已付款，2为已发货，3为成功',
  `be_has_gfinish` int(1) NOT NULL DEFAULT '0' COMMENT '是否已成功收货过0否1是',
  `zong_has_gfinish` int(1) NOT NULL DEFAULT '0' COMMENT '是否已成功收货过0否1是',
  `sendtype` tinyint(1) unsigned NOT NULL COMMENT '0为快递，1为自提',
  `paytype` tinyint(1) NOT NULL COMMENT '1为余额，2为在线，3为到付',
  `paytypecode` varchar(30) NOT NULL COMMENT '0货到付款，1微支付，2支付宝付款，3余额支付，4积分支付',
  `paytypename` varchar(50) NOT NULL,
  `transid` varchar(50) NOT NULL DEFAULT '0' COMMENT '外部单号(微支付单号等)',
  `remark` varchar(1000) DEFAULT '',
  `zong_remark` varchar(1000) DEFAULT '',
  `be_remark` varchar(1000) DEFAULT '',
  `expresscom` varchar(30) NOT NULL,
  `expresssn` varchar(50) NOT NULL,
  `express` varchar(30) NOT NULL,
  `be_expresscom` varchar(30) NOT NULL,
  `be_expresssn` varchar(50) NOT NULL,
  `be_express` varchar(30) NOT NULL,
  `addressid` int(10) unsigned NOT NULL,
  `be_goodsprice` decimal(10,2) DEFAULT '0.00',
  `zong_goodsprice` decimal(10,2) DEFAULT '0.00',
  `dispatchprice` decimal(10,2) DEFAULT '0.00',
  `zong_dispatchprice` decimal(10,2) DEFAULT '0.00',
  `dispatchexpress` varchar(50) DEFAULT '',
  `dispatch` int(10) DEFAULT '0',
  `dispatch_name` varchar(50) DEFAULT '',
  `be_dispatchprice` decimal(10,2) DEFAULT '0.00',
  `be_dispatchexpress` varchar(50) DEFAULT '',
  `be_dispatch_name` varchar(50) DEFAULT '',
  `be_dispatch` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `address_address` varchar(100) NOT NULL,
  `address_area` varchar(10) NOT NULL,
  `address_city` varchar(10) NOT NULL,
  `address_province` varchar(10) NOT NULL,
  `address_realname` varchar(10) NOT NULL,
  `address_mobile` varchar(20) NOT NULL,
  `rsreson` varchar(500) DEFAULT '' COMMENT '退货款退原因',
  `be_rsreson` varchar(500) DEFAULT '' COMMENT '退货款退原因',
  `isrest` int(1) NOT NULL DEFAULT '0',
  `paytime` int(10) DEFAULT '0' COMMENT '订单支付时间',
  `updatetime` int(10) DEFAULT '0' COMMENT '订单更新时间',
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '含总部商品1为总部',
  `is_be` int(1) NOT NULL DEFAULT '0' COMMENT '含分部订单',
  `area` varchar(30) DEFAULT '' COMMENT '区',
  `visual_pay` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否是虚拟付款0否1是，虚拟付款即后台付款',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_order
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_order_goods`
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_goods`;
CREATE TABLE `shop_order_goods` (
  `goodssn` varchar(50) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `beid` int(10) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderid` int(10) unsigned NOT NULL,
  `goodsid` int(10) unsigned NOT NULL,
  `status` tinyint(3) DEFAULT '0' COMMENT '0为无状态,1已收货,-3换货中,-7换货后已发货, -4退货中, -5已退货, -6已退款',
  `restatus` tinyint(3) DEFAULT '0' COMMENT '0未换货，1换货',
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `rsreson` text,
  `return_expresscom` varchar(30) NOT NULL,
  `return_expresssn` varchar(50) NOT NULL,
  `return_express` varchar(30) NOT NULL,
  `returnmoney` decimal(10,2) DEFAULT '0.00',
  `returnmoneytype` int(1) NOT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `total` int(10) unsigned NOT NULL DEFAULT '1',
  `optionid` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `optionname` text,
  `be_return_money` int(1) DEFAULT '0' COMMENT '0否1是 分店是否退款',
  `iscomment` int(1) DEFAULT '0' COMMENT '是否已评论0否1是',
  `is_system` int(1) NOT NULL DEFAULT '0' COMMENT '1为总部商品',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_order_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `shop_order_paylog`
-- ----------------------------
DROP TABLE IF EXISTS `shop_order_paylog`;
CREATE TABLE `shop_order_paylog` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `orderid` int(10) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `pid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shop_order_paylog
-- ----------------------------

-- ----------------------------
-- Table structure for `sms_cache`
-- ----------------------------
DROP TABLE IF EXISTS `sms_cache`;
CREATE TABLE `sms_cache` (
  `beid` int(10) NOT NULL,
  `cachetime` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `checkcount` int(3) NOT NULL,
  `smstype` varchar(50) DEFAULT NULL,
  `tell` varchar(50) DEFAULT NULL,
  `openid` varchar(50) DEFAULT NULL,
  `vcode` varchar(50) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sms_cache
-- ----------------------------

-- ----------------------------
-- Table structure for `system_config`
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(100) NOT NULL COMMENT '配置名称',
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES ('1', 'system_website', 'baijiacms.cc');
INSERT INTO `system_config` VALUES ('2', 'system_config_cache', 'a:2:{s:14:\"system_website\";s:12:\"baijiacms.cc\";s:19:\"system_config_cache\";s:0:\"\";}');

-- ----------------------------
-- Table structure for `system_store`
-- ----------------------------
DROP TABLE IF EXISTS `system_store`;
CREATE TABLE `system_store` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `compid` int(11) NOT NULL,
  `saleid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `logo` varchar(1000) DEFAULT NULL,
  `sname` varchar(100) NOT NULL,
  `is_system` int(1) NOT NULL DEFAULT '0',
  `isclose` int(1) NOT NULL,
  `fullwebsite` varchar(200) NOT NULL,
  `website` varchar(100) NOT NULL,
  `website2` varchar(100) NOT NULL,
  `website3` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_store
-- ----------------------------
INSERT INTO `system_store` VALUES ('1', '0', '0', '1491293199', '0', null, 'tt.baijiacms.cc', '0', '0', 'http://baijiacms.cc/', 'tt.baijiacms.cc', '', '');

-- ----------------------------
-- Table structure for `thirdlogin`
-- ----------------------------
DROP TABLE IF EXISTS `thirdlogin`;
CREATE TABLE `thirdlogin` (
  `beid` int(10) NOT NULL,
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(120) NOT NULL DEFAULT '',
  `desc` text NOT NULL,
  `configs` text NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of thirdlogin
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `loginkey` varchar(20) NOT NULL,
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `is_admin` int(1) NOT NULL DEFAULT '0' COMMENT '1管理员0普用户',
  `username` varchar(50) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('', '1', '1491289856', '21232f297a57a5a743894a0e4a801fc3', '0', 'admin', '1');
INSERT INTO `user` VALUES ('', '0', '1491290124', '21232f297a57a5a743894a0e4a801fc3', '1', 'admin', '2');

-- ----------------------------
-- Table structure for `weixin_rule`
-- ----------------------------
DROP TABLE IF EXISTS `weixin_rule`;
CREATE TABLE `weixin_rule` (
  `beid` int(10) NOT NULL,
  `url` varchar(500) NOT NULL,
  `thumb` varchar(60) NOT NULL,
  `keywords` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `ruletype` int(11) NOT NULL COMMENT '1文本回复 2图文回复',
  `addonsrule` int(1) NOT NULL DEFAULT '0' COMMENT '0常规，1模块规则',
  `addonsModule` varchar(50) DEFAULT '' COMMENT '所属模块',
  `content` text,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixin_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `weixin_wxfans`
-- ----------------------------
DROP TABLE IF EXISTS `weixin_wxfans`;
CREATE TABLE `weixin_wxfans` (
  `beid` int(10) NOT NULL,
  `createtime` int(10) NOT NULL DEFAULT '0',
  `openid` varchar(50) DEFAULT NULL,
  `weixin_openid` varchar(100) NOT NULL,
  `follow` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否订阅',
  `nickname` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(200) NOT NULL DEFAULT '',
  `gender` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别(0:保密 1:男 2:女)',
  `longitude` decimal(10,2) DEFAULT '0.00' COMMENT '地理位置经度',
  `latitude` decimal(10,2) DEFAULT '0.00' COMMENT '地理位置纬度',
  `precision` decimal(10,2) DEFAULT '0.00' COMMENT '地理位置精度',
  PRIMARY KEY (`beid`,`weixin_openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixin_wxfans
-- ----------------------------
