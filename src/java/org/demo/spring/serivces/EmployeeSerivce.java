/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.demo.spring.serivces;

import org.demo.spring.bean.EmployeeBean;

/**
 *
 * @author SFB
 */
public interface  EmployeeSerivce {
    
    public abstract int save(EmployeeBean employee);
    
    public abstract int update(EmployeeBean employee);
    
    public abstract int deleted(EmployeeBean employee);
    
    public abstract EmployeeBean find(EmployeeBean employee);
	
	public abstract EmployeeBean login(String username, String password);
	
    
}
