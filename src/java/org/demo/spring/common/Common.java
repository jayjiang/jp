/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.demo.spring.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

/**
 *
 * @author SFB
 */
public class Common {
    
    //生成随机数字和字母
    public static String getSecret(int length){
        String val = "";  
        Random random = new Random();  
        //参数length，表示生成几位随机数  
        for(int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";  
            //输出字母还是数字  
            if( "char".equalsIgnoreCase(charOrNum) ) {  
                //输出是大写字母还是小写字母  
                int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;  
                val += (char)(random.nextInt(26) + temp);  
            } else if( "num".equalsIgnoreCase(charOrNum) ) {  
                val += String.valueOf(random.nextInt(10));  
            }  
        }  
        return val;  
    }
    
    public static ModelAndView returnJson(String msg,int code,Object data,String k){
        Map map=new HashMap(); 
        map.put("msg", msg);
        map.put("code", code);
        map.put("data", data);
		return new ModelAndView(new MappingJacksonJsonView(),map); 
       // return map;
    }
	
	public static boolean isEmpty(Object obj)  
    {  
        if (obj == null)  {  
            return true;  
        }  
        if ((obj instanceof List))  {  
            return ((List) obj).isEmpty();  
        }  
        if ((obj instanceof String))   {  
            return ((String) obj).trim().equals("");  
        }  
        return false;  
    }  
      
    /** 
     * 判断对象不为空 
     *  
     * @param obj 
     *            对象名 
     * @return 是否不为空 
     */  
    public static boolean isNotEmpty(Object obj)  
    {  
        return !isEmpty(obj);  
    }
	
	
    
    
    
    
}
